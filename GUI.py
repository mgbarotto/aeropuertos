import tkinter as tk
from tkinter import ttk
from MapPlot import Map


class GUI(tk.Frame):
    """
    Una clase que representa la ventana principal del programa y todos sus
    elementos
    """
    def __init__(self, parent, grafo, bg="#00a4ff"):
        """
        Constructor
            parent: debe ser un widget de TKInter
            grafo: el grafo con la informacion del mapa
            bg: color de fondo de la ventana
        """
        # cnv = canvas
        # lbl = label
        # rdb = radiobutton
        # cmb = combobox
        self.__parent = parent
        self.__bg = bg
        self.__grafo = grafo
        tk.Frame.__init__(self, parent, bg=self.__bg)
        self.__mapa = Map(self, "bottom", bg)
        self.__destino = None
        self.__origen = None

        # Canvas que contiene toda la barra superior
        self.__cnv_top = tk.Canvas(self, bg=self.__bg, highlightthickness=0)
        self.__cnv_top.pack(side="top")
        # Canvas que contiene los canvas de seleccion de origen y destino
        self.__cnv_comboboxes = tk.Canvas(self.__cnv_top, bg=self.__bg,
                                          highlightthickness=0)
        self.__cnv_comboboxes.pack(side="left")

        # Canvas que contiene los botones radio de seleccion
        self.__cnv_opciones = tk.Canvas(self.__cnv_comboboxes, bg=self.__bg,
                                        highlightthickness=0)
        self.__cnv_opciones.pack(side="right")

        # Criterio por el cual se busca
        self.__crit = tk.IntVar()
        self.__lbl_criterio = tk.Label(self.__cnv_opciones, bg=self.__bg,
                                       text="Criterio de busqueda: ", bd=0,
                                       highlightthickness=0)
        self.__lbl_criterio.pack(anchor=tk.W)
        self.__rdb_escalas = tk.Radiobutton(self.__cnv_opciones, bg=self.__bg,
                                            text="escalas", value=0,
                                            variable=self.__crit)
        self.__rdb_escalas.pack(anchor=tk.W)
        self.__rdb_escalas.select()
        self.__rdb_precio = tk.Radiobutton(self.__cnv_opciones, text="precio",
                                           bg=self.__bg, variable=self.__crit,
                                           highlightcolor=self.__bg, value=1)
        self.__rdb_precio.pack(anchor=tk.W)
        self.__rdb_dist = tk.Radiobutton(self.__cnv_opciones, text="dist.",
                                         bg=self.__bg, variable=self.__crit,
                                         value=2)
        self.__rdb_dist.pack(anchor=tk.W)
        # Canvas que contiene la seleccion de origen
        self.__cnv_orig = tk.Canvas(self.__cnv_comboboxes, bg=self.__bg,
                                    highlightthickness=0)
        self.__cnv_orig.pack(side="top")
        # Etiqueta "Origen:"
        self.__lbl_orig = tk.Label(self.__cnv_orig, background=self.__bg,
                                   text="Origen: ", width=20, bd=0,
                                   highlightthickness=0)
        self.__lbl_orig.pack(side="left")
        # Combobox para elegir pais de origen
        self.__cmb_orig_pais = ttk.Combobox(self.__cnv_orig, state="readonly",
                                            values=grafo.data.paises)
        self.__cmb_orig_pais.bind("<<ComboboxSelected>>",
                                  self._pais_orig_cambio)
        self.__cmb_orig_pais.set("Elija pais.")
        self.__cmb_orig_pais.pack(side="top")
        # Combobox para elegir ciudad de origen
        self.__cmb_orig_ciudad = ttk.Combobox(self.__cnv_orig,
                                              state="disabled")
        self.__cmb_orig_ciudad.pack(side="top")
        self.__cmb_orig_ciudad.bind("<<ComboboxSelected>>",
                                    self._ciudad_orig_cambio)
        # Combobox para elegir aeropuerto de origen
        self.__cmb_orig_puerto = ttk.Combobox(self.__cnv_orig,
                                              state="disabled")
        self.__cmb_orig_puerto.pack(side="top")
        self.__cmb_orig_puerto.bind("<<ComboboxSelected>>",
                                    self._puerto_orig_cambio)
        # Canvas que contiene la seleccion de destino
        self.__cnv_dest = tk.Canvas(self.__cnv_comboboxes, bg=self.__bg,
                                    highlightthickness=0)
        self.__cnv_dest.pack(side="top")
        # Etiqueta "Destino:"
        self.__lbl_dest = tk.Label(self.__cnv_dest, background=self.__bg,
                                   text="Destino: ", width=20, bd=0,
                                   highlightthickness=0)
        self.__lbl_dest.pack(side="left")
        # Combobox para elegir pais de destino
        self.__cmb_dest_pais = ttk.Combobox(self.__cnv_dest, state="readonly",
                                            values=grafo.data.paises)
        self.__cmb_dest_pais.bind("<<ComboboxSelected>>",
                                  self._pais_dest_cambio)
        self.__cmb_dest_pais.set("Elija pais.")
        self.__cmb_dest_pais.pack(side="top")
        # Combobox para elegir ciudad de destino
        self.__cmb_dest_ciudad = ttk.Combobox(self.__cnv_dest,
                                              state="disabled")
        self.__cmb_dest_ciudad.pack(side="top")
        self.__cmb_dest_ciudad.bind("<<ComboboxSelected>>",
                                    self._ciudad_dest_cambio)
        # Combobox para elegir aeropuerto de destino
        self.__cmb_dest_puerto = ttk.Combobox(self.__cnv_dest,
                                              state="disabled")
        self.__cmb_dest_puerto.pack(side="top")
        self.__cmb_dest_puerto.bind("<<ComboboxSelected>>",
                                    self._puerto_dest_cambio)
        # Boton OK
        self.__btn_OK = tk.Button(self, text="OK", command=self._ok_click)
        self.__btn_OK.pack(side="top")

        # Etiqueta de informacion general
        self.__lbl_mensaje = tk.Label(self.__cnv_top, background=self.__bg,
                                      text="", width=40, bd=0,
                                      highlightthickness=0, justify="left")
        self.__lbl_mensaje.pack(side="right")

    def _ok_click(self):
        """
        Handler que se llama cuando se hace click en el boton OK
        """
        self._try_draw()

    def _pais_orig_cambio(self, event):
        """
        Handler que se llama cuando se cambia el Combobox de pais
            de origen
        """
        sel = self.__cmb_orig_pais['values'][self.__cmb_orig_pais.current()]
        data = self.__grafo.data
        self.__cmb_orig_ciudad.config(values=data.ciudades_por_pais[sel],
                                      state="readonly")
        self.__cmb_orig_ciudad.set("Elija ciudad.")
        self.__cmb_orig_puerto.set("")
        self.__cmb_orig_puerto.config(state="disabled")
        self.__origen = None

    def _ciudad_orig_cambio(self, event):
        """
        Handler que se llama cuando se cambia el Combobox de ciudad
            de origen
        """
        current_c = self.__cmb_orig_ciudad.current()
        current_p = self.__cmb_orig_pais.current()
        ciudad = self.__cmb_orig_ciudad['values'][current_c]
        pais = self.__cmb_orig_pais['values'][current_p]
        data = self.__grafo.data
        aeropuertos = []
        for a in data.aeropuertos_por_ciudad[ciudad + pais]:
            aeropuertos.append(a[1])
        self.__cmb_orig_puerto.config(values=aeropuertos,
                                      state="readonly")
        self.__cmb_orig_puerto.set("Elija aeropuerto.")
        self.__origen = None

    def _puerto_orig_cambio(self, event):
        """
        Handler que se llama cuando se cambia el Combobox de aeropuerto
            de origen
        """
        current_c = self.__cmb_orig_ciudad.current()
        current_p = self.__cmb_orig_pais.current()
        ciudad = self.__cmb_orig_ciudad['values'][current_c]
        pais = self.__cmb_orig_pais['values'][current_p]
        data = self.__grafo.data
        actual = self.__cmb_orig_puerto.current()
        id_origen = data.aeropuertos_por_ciudad[ciudad + pais][actual][0]
        self.__origen = self.__grafo.data.id_to_airport(id_origen)

    def _pais_dest_cambio(self, event):
        """
        Handler que se llama cuando se cambia el Combobox de pais
            de destino
        """
        sel = self.__cmb_dest_pais['values'][self.__cmb_dest_pais.current()]
        data = self.__grafo.data
        self.__cmb_dest_ciudad.config(values=data.ciudades_por_pais[sel],
                                      state="readonly")
        self.__cmb_dest_ciudad.set("Elija ciudad.")
        self.__cmb_dest_puerto.set("")
        self.__cmb_dest_puerto.config(state="disabled")
        self.__destino = None

    def _ciudad_dest_cambio(self, event):
        """
        Handler que se llama cuando se cambia el Combobox de ciudad
            de destino
        """
        current_c = self.__cmb_dest_ciudad.current()
        current_p = self.__cmb_dest_pais.current()
        ciudad = self.__cmb_dest_ciudad['values'][current_c]
        pais = self.__cmb_dest_pais['values'][current_p]
        data = self.__grafo.data
        aeropuertos = []
        for a in data.aeropuertos_por_ciudad[ciudad + pais]:
            aeropuertos.append(a[1])
        self.__cmb_dest_puerto.config(values=aeropuertos,
                                      state="readonly")
        self.__cmb_dest_puerto.set("Elija aeropuerto.")
        self.__destino = None

    def _puerto_dest_cambio(self, event):
        """
        Handler que se llama cuando se cambia el Combobox de aeropuerto
            de destino
        """
        current_c = self.__cmb_dest_ciudad.current()
        current_p = self.__cmb_dest_pais.current()
        ciudad = self.__cmb_dest_ciudad['values'][current_c]
        pais = self.__cmb_dest_pais['values'][current_p]
        data = self.__grafo.data
        actual = self.__cmb_dest_puerto.current()
        id_destino = data.aeropuertos_por_ciudad[ciudad + pais][actual][0]
        self.__destino = self.__grafo.data.id_to_airport(id_destino)

    def _try_draw(self):
        """
        Intenta dibujar algo en el mapa.
        Si hay solo un origen seleccionado dibuja el origen y todos sus nodos
            adyacentes
        Si hay un destino y un origen, dibuja un camino si lo encuentra, o
            limpia el mapa si no lo encuentra
        """
        if self.__origen and self.__destino:
            if self.__crit.get() == 0:
                camino = self.__grafo.camino_bfs(self.__origen, self.__destino)
            else:
                if self.__crit.get() == 1:
                    self.__grafo.peso_por_precio()
                if self.__crit.get() == 2:
                    self.__grafo.peso_por_dist()
                camino = self.__grafo.camino_dijkstra(self.__origen,
                                                      self.__destino)
            self.__mapa.graficar(camino)
            if not camino:
                self.__lbl_mensaje.config(text="No se encontro un camino.")
            else:
                precio_total = 0
                dist_total = 0
                texto = "\nCamino encontrado:\n"
                for n in reversed(camino.V):
                    texto = texto + n.dato.code + " - " + n.dato.nombre + "\n"
                    if n.adj:
                        texto = texto + " | " + str(round(n.adj[0].distancia))\
                            + " km, $" + str(round(n.adj[0].precio, 2)) + "\n"
                        precio_total += n.adj[0].precio
                        dist_total += n.adj[0].distancia
                texto = texto + "Precio: $" + str(round(precio_total, 2)) +\
                    ".\nDistancia total: " + str(round(dist_total)) + "km."
                self.__lbl_mensaje.config(text=texto)
        elif self.__origen:
            # Si hay origen pero no destino, graficar todos los
            #  directamente conectados
            self.__mapa.graficar(self.__grafo.grafo_bfs(self.__origen, 1))
        else:
            self.__lbl_mensaje.config(text="Falta aeropuerto de origen")
