import matplotlib
matplotlib.use('TkAgg')
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap


colors = ['#12c000', '#ffe400', '#ff9600', '#e30000']


class Map:
    """
    Clase que representa un mapa en una ventana de TKInter, con
        funciones para dibujar grafos
    """
    DEF_MIN_LON = -180
    DEF_MAX_LON = 180
    DEF_MIN_LAT = -60
    DEF_MAX_LAT = 60
    def __init__(self, parent_canvas, map_side="right", canvas_bg="#FFF157"):
        self.min_lon = Map.DEF_MIN_LON
        self.max_lon = Map.DEF_MAX_LON
        self.min_lat = Map.DEF_MIN_LAT
        self.max_lat = Map.DEF_MAX_LAT
        self.figure = Figure(facecolor=canvas_bg, edgecolor=canvas_bg,
                             tight_layout=True)
        self.axes = self.figure.add_subplot(111)
        self.map = None
        self.canvas = FigureCanvasTkAgg(self.figure, master=parent_canvas)
        self.update()
        self.canvas.get_tk_widget().configure(bg=canvas_bg,
                                              highlightthickness=0)
        self.canvas.get_tk_widget().pack(side=map_side, fill="both",
                                         expand=True)

    def clear(self):
        """
        Limpia los nodos y texto dibujados en el mapa
        """
        self.axes.clear()
        plt.clf()

    def update(self):
        """
        Acota a la seccion delimitada entre las latituted y
            longitudes minimas y maximas
        """
        self.map = Basemap(llcrnrlat=self.min_lat, llcrnrlon=self.min_lon,
                           urcrnrlat=self.max_lat, urcrnrlon=self.max_lon,
                           resolution='l', projection='merc', ax=self.axes)
        self.map.drawcoastlines()
        self.map.drawcountries()
        self.map.drawstates()
        self.map.fillcontinents(color="#eaeaea")

    def graficar(self, grafo, lineas=True, clear=True):
        """
        Grafica el grafo pasado por parametro
        lineas: booleano que indica si se dibujan las lineas de conexion
            o solo los nodos del grafo
        clear: indica si se limpia el mapa antes de redibujarlo
        """
        if clear:
            # Limpia el mapa
            self.clear()
        if not grafo:  # Si el grafo que se quiere graficar no existe
            # Reinicia a valores por defecto
            self.min_lon = Map.DEF_MIN_LON
            self.max_lon = Map.DEF_MAX_LON
            self.min_lat = Map.DEF_MIN_LAT
            self.max_lat = Map.DEF_MAX_LAT
            # Redibuja el mapa
            self.update()
            self.canvas.draw()
            return
        self.calcular_rango(grafo.V)
        self.update()
        if lineas:
            for n in grafo.V:
                for r in n.adj:
                    self.map.drawgreatcircle(lat1=n.dato.lat, lon1=n.dato.lon,
                                             lat2=r.destino.dato.lat,
                                             lon2=r.destino.dato.lon,
                                             linewidth=2,
                                             color=colors[0])
        for n in grafo.V:
            x, y = self.map(float(n.dato.lon), float(n.dato.lat))
            if n.dato.code:
                self.map.plot(x, y, 'wo', markersize=25.0)
                self.axes.text(x, y, n.dato.code, fontsize=9, ha='center',
                               va='center', color='k')
            else:
                self.map.plot(x, y, 'ko', markersize=5.0)
        self.canvas.draw()

    def calcular_rango(self, nodos):
        """
        Calcula la seccion del mapa que debe mostrar para mostrar todos los
            nodos
         en la lista
        """
        tmp_lon_array = [float(x.dato.lon) for x in nodos]
        self.min_lon = min(tmp_lon_array) - 5.
        self.max_lon = max(tmp_lon_array) + 5.
        tmp_lat_array = [float(x.dato.lat) for x in nodos]
        self.min_lat = max(min(tmp_lat_array) - 2., -85)
        self.max_lat = min(max(tmp_lat_array) + 2., 85)
        # Normalizar
        relacion_lat = ((self.max_lat - self.min_lat) /
                        (Map.DEF_MAX_LAT - Map.DEF_MIN_LAT))

        relacion_lon = ((self.max_lon - self.min_lon) /
                        (Map.DEF_MAX_LON - Map.DEF_MIN_LON))
        if relacion_lat == relacion_lon:
            return
        elif relacion_lat > relacion_lon:
            # cambiar la proporcion de lon
            # x / (MAX_LON - MIN_LON)) = relacion_lat
            # x = relacion_lon * MAX_LON-MIN_LON
            # (self.max_lon - self.min_lon) = relacion lat * MAX_LON-MIN_LON
            lon_abarcada = relacion_lat * (Map.DEF_MAX_LON - Map.DEF_MIN_LON)
            dif_lon = lon_abarcada - (self.max_lon - self.min_lon)
            self.max_lon += dif_lon / 2
            self.min_lon -= dif_lon / 2
            relacion_lon = ((self.max_lon - self.min_lon) /
                            (Map.DEF_MAX_LON - Map.DEF_MIN_LON))
        else:
            # cambiar la proporcion de lat
            # x / (MAX_LAT - MIN_LAT)) = relacion_lon
            # x = relacion_lon * MAX_LAT-MIN_LAT
            # (self.max_lat - self.min_lat) = relacion lat * MAX_LAT-MIN_LAT
            lat_abarcada = relacion_lon * (Map.DEF_MAX_LAT - Map.DEF_MIN_LAT)
            dif_lat = lat_abarcada - (self.max_lat - self.min_lat)
            self.max_lat += dif_lat / 2
            self.min_lat -= dif_lat / 2
            relacion_lat = ((self.max_lat - self.min_lat) /
                            (Map.DEF_MAX_LAT - Map.DEF_MIN_LAT))
