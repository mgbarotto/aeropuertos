import sys
import random
import queue
import heapq
from Data import Data


INFINITO = sys.maxsize
BLANCO, GRIS, NEGRO = 0, 1, 2


class Grafo:
    def __init__(self):
        """
        Crea un grafo vacio, con un campo "data" que se usa para llenarlo
         desde un archivo
        """
        self.data = Data(self)
        self.V = []
        self.time = 0

    def agregar_nodo(self, dato):
        """
        Agrega un nodo usando el dato pasado por parametro como dato
        Devuelve el nodo creado
        """
        nodo = Nodo(dato)
        self.V.append(nodo)
        return nodo

    def vincular(self, origen, destino):
        """
        Genera un vinculo (ruta) entre los dos nodos que se pasan por parametro
        El vinculo va desde origen hacia destino
        """
        if not isinstance(origen, Nodo):
            origen = self.data.id_to_airport(origen)
        if not isinstance(destino, Nodo):
            destino = self.data.id_to_airport(destino)
        distancia = Data.distance_on_unit_sphere(origen.dato.lat,
                                                 origen.dato.lon,
                                                 destino.dato.lat,
                                                 destino.dato.lon)

        origen.adj.append(Ruta(destino, distancia))

    def vincular_bidireccional(self, n1, n2):
        """
        Genera un vinculo (ruta) bidireccional entre los dos nodos que se
        pasan por parametro
        """
        self.vincular(n1, n2)
        self.vincular(n2, n1)

    def bfs(self, s):
        """
        Busqueda en anchura (Breadth First Search) del grafo, iniciando
        en el nodo que se pasa por parametro
        """
        for u in self.V:
            u.color = BLANCO
            u.dist = INFINITO
            u.ancestro = None
        s.color = GRIS
        s.dist = 0
        q = queue.Queue()
        q.put(s)
        while not q.empty():
            u = q.get()
            for v in u.adj:
                if v.destino.color == BLANCO:
                    v.destino.color = GRIS
                    v.destino.dist = u.dist + 1
                    v.destino.ancestro = u
                    q.put(v.destino)
            u.color = NEGRO

    def grafo_bfs(self, s, dist=-1):
        """
        Devuelve un grafo que contiene el origen y los nodos adyacentes
        a un nivel igual a dist.
        Por defecto, dist = -1, que indica distancia infinita.
        """
        g = Grafo()
        for u in self.V:
            u.color = BLANCO
            u.dist = INFINITO
            u.ancestro = None
        s.color = GRIS
        s.dist = 0
        q = queue.Queue()
        q.put(s)

        s_nodo = g.agregar_nodo(s.dato)
        qn = queue.Queue()
        qn.put(s_nodo)

        while not q.empty():
            u = q.get()
            u_nodo = qn.get()
            for v in u.adj:
                if v.destino.color == BLANCO:
                    v.destino.color = GRIS
                    v.destino.dist = u.dist + 1
                    v.destino.ancestro = u
                    v_nodo = g.agregar_nodo(v.destino.dato)
                    v_nodo.color = GRIS
                    v_nodo.dist = u_nodo.dist + 1
                    v_nodo.ancestro = u_nodo
                    g.vincular(u_nodo, v_nodo)
                    if v_nodo.dist < dist or dist == -1:
                        q.put(v.destino)
                        qn.put(v_nodo)
            u.color = NEGRO
        return g

    def camino_bfs(self, origen, destino):
        """
        Develve un grafo que consiste de un camino (calculado mediante BFS)
        desde el origen al destino.
        Si el camino no existe, devuelve None.
        """
        self.bfs(origen)
        return self._camino(origen, destino)

    def camino_dijkstra(self, origen, destino):
        """
        Develve un grafo que consiste de un camino (calculado mediante
        dijkstra) desde el origen al destino.
        Si el camino no existe, devuelve None.
        """
        self.dijkstra(origen)
        return self._camino(origen, destino)

    def _camino(self, origen, destino):
        """
        Devuelve un grafo que representa un camino desde el origen al destino.
        Si el camino no existe, devuelve None.
        Esta funcion es usada por las funciones camino_dijkstra y camino_bfs.
        No deberia usarse por si misma
        """
        if not destino.ancestro:
            return None  # No hay camino
        camino = Grafo()
        nodo = destino
        nodo_nuevo = camino.agregar_nodo(destino.dato)
        while nodo.ancestro:
            nodo_ancestro_nuevo = camino.agregar_nodo(nodo.ancestro.dato)
            camino.vincular(nodo_ancestro_nuevo, nodo_nuevo)
            nodo_nuevo = nodo_ancestro_nuevo
            nodo = nodo.ancestro
        return camino

    def dfs(self):
        """
        Busqueda en profundidad (Depth First Search) del grafo.
        Devuelve un booleano que indica si el grafo tiene ciclos
        """
        ciclo = False
        for u in self.V:
            u.color = BLANCO
            u.ancestro = None
        self.time = 0
        for u in self.V:
            if u.color == BLANCO:
                ciclo = ciclo or self._dfs_visit(u)
        return ciclo

    def _dfs_visit(self, u):
        """
        Funcion auxiliar para dfs
        """
        ciclo = False
        self.time += 1
        u.timeD = self.time
        u.color = GRIS
        for v in u.adj:
            if v.color == BLANCO:
                v.ancestro = u
                self._dfs_visit(v)
            if v.color == GRIS:
                ciclo = True
        u.color = NEGRO
        self.time += 1
        u.timeF = self.time
        return ciclo

    def dijkstra(self, s):
        """
        Algoritmo de Dijkstra, usando el miebro "peso" del nodo como peso
        para generar caminos mas cortos empezando en el nodo pasado
        por parametro
        """
        for n in self.V:
            n.ancestro = None
            n.dist = INFINITO
        s.dist = 0
        cola = self.V[:]
        heapq.heapify(cola)
        while cola:
            u = heapq.heappop(cola)
            for r in u.adj:
                self._dijk_relax(u, r.destino, r.peso)

    def _dijk_relax(self, u, v, w):
        """
        Funcion auxiliar de dijkstra.
        No deberia usarse por si misma
        """
        if v.dist > (u.dist + w):
            v.dist = u.dist + w
            v.ancestro = u

    def peso_por_precio(self):
        """
        Prepara los nodos del grafo para usar el precio de las aristas como
            peso
        en el algoritmo de Dijkstra
        """
        for n in self.V:
            for r in n.adj:
                r.peso_por_precio()

    def peso_por_dist(self):
        """
        Prepara los nodos del grafo para usar la distancia de las aristas como
            peso
        en el algoritmo de Dijkstra
        """
        for n in self.V:
            for r in n.adj:
                r.peso_por_dist()


class Nodo:
    """
    Clase que contiene la informacion necesaria para los algoritmos de grafo.
    """
    def __init__(self, dato):
        """
        Crea un nodo con valores por defecto
        ancestro = None
        distancia = INFINITO
        color = BLANCO
        timeD = timeF = 0
        adyacentes = []
        La lista de adyacentes debe llenarse con instancias del tipo Arista
            o derivados
        Y con un dato que se pasa por parametro.
        """
        self.dato = dato

        self.ancestro = None
        self.dist = INFINITO
        self.color = BLANCO
        self.timeD = 0
        self.timeF = 0

        self.adj = []

    def __lt__(self, obj):
        return self.dist < obj.dist


class Arista:
    """
    Conexion entre un nodo origen que la contiene en su lista de adyacencia
        y un nodo destino referenciado en la variable destino
    Contiene una variable de peso que se utiliza en algunos algoritmos
        de grafo
    """
    def __init__(self, destino, peso=1):
        """
        Cre una arista que lleva a destino, el peso por defecto es 1
        """
        self.destino = destino
        self.peso = peso

    def set_peso(self, peso):
        self.peso = peso


class Ruta(Arista):
    """
    Clase hija de arista, con funciones y miembros especificos para el uso con
        aeropuertos
    """
    def __init__(self, destino, distancia):
        """
        Inicializa una arista, inicialmente usando la distancia como peso.
        Genera un precio para el viaje basado en la distancia.
        """
        Arista.__init__(self, destino, distancia)
        self.distancia = distancia
        # Usa la distancia como seed
        # Asi, un mismo viaje siempre tiene el mismo precio
        random.seed(distancia)
        # Calcula un precio al azar entre un 50% y un 125% de la distancia
        self.precio = distancia * random.randrange(50, 125, 1)/100

    def peso_por_precio(self):
        """
        Prepara la arista para que utilice el precio como peso.
        La forma correcta de uso es llamar a la funcion equivalente
            en el grafo.
        """
        self.peso = self.precio

    def peso_por_dist(self):
        """
        Prepara la arista para que utilice la distancia como peso.
        La forma correcta de uso es llamar a la funcion equivalente
            en el grafo.
        """
        self.peso = self.distancia
