﻿"""
Modulo que incluye lo necesario para llenar el grafo
    a partir de un archivo de datos de aeropuertos y de rutas
"""
import math


class Aeropuerto:
    """
    Clase que envuelve los datos del aeropuerto, sirve como dato de la clase
        nodo del grafo
    """
    def __init__(self, code, nombre, lat, lon):
        """
        Crea un aerpuerto con el codigo, nombre, latitud y longitud
            pasados por parametro
        """
        self.code = code
        self.nombre = nombre
        self.lat = lat
        self.lon = lon


class Data:
    """
    Clase que incluye un parser y datos utiles sobre los aeropuertos, paises y
        ciudades.
    """
    def __init__(self, grafo):
        """
        Crea una instancia del tipo Data, que guardara sus datos en el grafo
            pasado por parametro.
        """
        self.__airport_by_id = {}
        self.grafo = grafo
        self.paises = []
        self.ciudades_por_pais = {}
        self.aeropuertos_por_ciudad = {}

    def id_to_airport(self, airport_id):
        """
        Devuelve el nodo correspondiente al aeropuerto con el id que se pasa
            por parametro
        """
        assert airport_id in self.__airport_by_id, ("ID invalido:", airport_id)
        return self.__airport_by_id[airport_id]

    def parse_airports(self, pais_filtro=None, ports_file='Data/airports.dat',
                       routes_file='Data/routes.dat'):
        """
        Rellena el grafo con los datos de los archivos

        pais_filtro -- Si se especifica, permite filtrar el grafo para que
            solo contenga datos de un pais
        ports_file -- Indica un archivo con datos de aeropuerto
        routes_file -- Indica un archivo con datos de rutas
        """
        self.grafo.V = []

        with open(ports_file) as airports:
            for line in airports:
                # Separa los datos en una lista y borra las comillas restantes
                data = [x.replace("\"", "") for x in line.split(',')]
                # Filtra los aeropuertos que no correspondan al pais indicado
                if not pais_filtro or data[3] == pais_filtro:
                    airport_id = int(data[0])
                    nombre = data[1]
                    ciudad = data[2]
                    pais = data[3]
                    code = data[4]
                    # Normaliza las latitudes y longitudes
                    lat = float(data[6])
                    while lat < -90.0:
                        lat += 180.0
                    while lat > 90.0:
                        lat -= 180
                    lon = float(data[7])
                    while lon < -180.0:
                        lon += 360.0
                    while lon > 180.0:
                        lon -= 360.0
                    # Crea el aeropuerto
                    puerto = Aeropuerto(code, nombre, lat, lon)
                    # Agrega el aerpuerto como nodo del grafo
                    nodo = self.grafo.agregar_nodo(puerto)
                    # Agrega el nodo al diccionario de IDs
                    self.__airport_by_id[airport_id] = nodo
                    if pais not in self.paises:
                        self.paises.append(pais)
                        self.ciudades_por_pais[pais] = []
                    if ciudad not in self.ciudades_por_pais[pais]:
                        self.ciudades_por_pais[pais].append(ciudad)

                    if ciudad + pais not in self.aeropuertos_por_ciudad:
                        self.aeropuertos_por_ciudad[ciudad + pais] = []
                    self.aeropuertos_por_ciudad[ciudad + pais].\
                        append((airport_id, nombre))
            self.paises.sort()
            for pais in self.ciudades_por_pais.keys():
                self.ciudades_por_pais[pais].sort()

        with open(routes_file) as routes:
            for line in routes:
                data = line.split(',')
                # Algunas rutas llevan a aeropuertos sin id
                # (no estan en los nodos)
                if data[3] == '\\N' or data[5] == '\\N':
                    continue
                source_id = int(data[3])
                dest_id = int(data[5])

                if (source_id in self.__airport_by_id
                        and dest_id in self.__airport_by_id):
                    self.grafo.vincular(source_id, dest_id)

    @staticmethod
    def distance_on_unit_sphere(lat1, long1, lat2, long2):
        """
        Calcula y retorna la distancia en kilometros dados datos de latitud
            y longitud
        http://www.johndcook.com/blog/python_longitude_latitude/
        """
        # Convert latitude and longitude to
        # spherical coordinates in radians.
        deg_to_rad = math.pi / 180.0

        # phi = 90 - latitude
        phi1 = (90.0 - lat1) * deg_to_rad
        phi2 = (90.0 - lat2) * deg_to_rad

        # theta = longitude
        theta1 = long1 * deg_to_rad
        theta2 = long2 * deg_to_rad

        # Compute spherical distance from spherical coordinates.

        # For two locations in spherical coordinates
        # (1, theta, phi) and (1, theta', phi')
        # cosine( arc length ) =
        #    sin phi sin phi' cos(theta-theta') + cos phi cos phi'
        # distance = rho * arc length

        cos = (math.sin(phi1) * math.sin(phi2) * math.cos(theta1 - theta2) +
               math.cos(phi1) * math.cos(phi2))
        arc = math.acos(cos)

        # Multiplicado por el radio de la tierra en kilometros
        return arc * 6373
