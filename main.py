if __name__ == "__main__":
    import tkinter as tk
    from Grafo import Grafo
    from GUI import GUI

    G = Grafo()
    G.data.parse_airports()
    window_bg = "#00a4ff"
    # Inicializa una ventana TK
    root = tk.Tk()
    # Maximiza la ventana
    root.state('zoomed')
    root.title('Aeropuertos')
    root.iconbitmap('Icon.ico')
    root.config(bg=window_bg)
    GUI(root, G, "#eeeeee").pack(fill="both", expand=True)
    root.mainloop()
